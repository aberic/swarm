package comm

const (
	// ProductionEnv 是否生产环境，在生产环境下控制台不会输出任何日志
	ProductionEnv = "PRODUCTION"
	// LogDirEnv 日志文件目录
	LogDirEnv = "LOG_DIR"
	// LogFileMaxSizeEnv 每个日志文件保存的最大尺寸 单位：M
	LogFileMaxSizeEnv = "LOG_FILE_MAX_SIZE"
	// LogFileMaxAgeEnv 文件最多保存多少天
	LogFileMaxAgeEnv = "LOG_FILE_MAX_AGE"
	// LogUtcEnv CST & UTC 时间
	LogUtcEnv = "LOG_UTC"
	// LogLevelEnv 日志级别(debugLevel/infoLevel/warnLevel/ErrorLevel/panicLevel/fatalLevel)
	LogLevelEnv = "LOG_LEVEL"
	// WorkerEnv 是否为worker节点
	WorkerEnv = "WORKER"
)
