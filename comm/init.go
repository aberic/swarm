package comm

import (
	"github.com/aberic/gnomon"
	"github.com/aberic/gnomon/log"
	"os"
	"path/filepath"
)

var (
	Worker = false
	// 目录管理路径
	DirPath   = ""
	WorkPath  = gnomon.EnvGetD("WORK_PATH", "")
	StackYaml = gnomon.EnvGetD("STACK_YAML", "")
)

// InitLog 初始化log日志组件
//
// 全局main入口均可调用执行
func initLog() {
	log.Fit(
		gnomon.EnvGetD(LogLevelEnv, "Debug"),
		gnomon.EnvGetD(LogDirEnv, os.TempDir()),
		gnomon.EnvGetIntD(LogFileMaxSizeEnv, 1024),
		gnomon.EnvGetIntD(LogFileMaxAgeEnv, 7),
		gnomon.EnvGetBool(LogUtcEnv),
		gnomon.EnvGetBool(ProductionEnv))
}

func init() {
	if gnomon.StringIsEmpty(WorkPath) {
		panic("work path should be definition")
	}
	DirPath = gnomon.StringBuildSep(string(filepath.Separator), WorkPath, "chain")
	initLog()
	Worker = gnomon.EnvGetBool(WorkerEnv)
}
