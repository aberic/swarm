module gykjgit.dccnet.com.cn/chain/swarm

go 1.14

require (
	github.com/Azure/go-ansiterm v0.0.0-20170929234023-d6e3b3328b78 // indirect
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/aberic/gnomon v0.0.0-20200715063408-fe18dcb1682f
	github.com/aberic/proc v0.0.0-20200723060459-a5dcb18e1013
	github.com/containerd/fifo v0.0.0-20201026212402-0724c46b320c // indirect
	github.com/docker/docker v0.0.0-20200531234253-77e06fda0c94
	github.com/docker/go-connections v0.4.0
	github.com/docker/go-metrics v0.0.1 // indirect
	github.com/gorilla/mux v1.7.4 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/pkg/errors v0.9.1
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/time v0.0.0-20200416051211-89c76fbcd5d1 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	google.golang.org/genproto v0.0.0-20191108220845-16a3f7862a1a // indirect
	google.golang.org/grpc v1.30.0
	gopkg.in/yaml.v3 v3.0.0-20200506231410-2ff61e1afc86
	gotest.tools v2.2.0+incompatible
	gykjgit.dccnet.com.cn/chain/proto v0.0.0-20201229021457-b52a8c19ba8e
)

replace gykjgit.dccnet.com.cn/chain/proto => gitee.com/aberic/proto v0.0.0-20201229021457-b52a8c19ba8e
