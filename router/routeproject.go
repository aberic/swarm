package router

import (
	"context"
	"github.com/aberic/gnomon/grope"
	"github.com/pkg/errors"
	proto "gykjgit.dccnet.com.cn/chain/proto/swarm"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
	"net/http"
)

func RouteProject(hs *grope.GHttpServe) {
	// 仓库相关路由设置
	route := hs.Group("/project")
	route.Post("/deploy", projectDeploy)
}

func projectDeploy(ctx *grope.Context) {
	project := &proto.ReqProject{}
	if err := ctx.ReceiveJSON(project); nil != err {
		responseFailJSON(ctx, errors.Wrap(err, "body error"))
		return
	}
	if err := client.Obtain().Project().Deploy(context.Background(), project); nil != err {
		_ = ctx.ResponseJSON(http.StatusOK, &response{Code: CodeFail, ErrMsg: err.Error()})
		return
	}
	_ = ctx.ResponseJSON(http.StatusOK, &response{Code: CodeSuccess})
}
