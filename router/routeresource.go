package router

import (
	"context"
	"github.com/aberic/gnomon/grope"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
	"net/http"
)

func RouteResource(hs *grope.GHttpServe) {
	// 仓库相关路由设置
	route := hs.Group("/resource")
	route.Get("/info", info)
}

func info(ctx *grope.Context) {
	r, err := client.Obtain().Resource().Info(context.Background())
	if nil != err {
		responseFailJSON(ctx, err)
		return
	}
	_ = ctx.ResponseJSON(http.StatusOK, &response{Code: CodeSuccess, Data: r})
}
