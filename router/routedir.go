package router

import (
	"github.com/aberic/gnomon"
	"github.com/aberic/gnomon/grope"
	"github.com/aberic/gnomon/log"
	proto "gykjgit.dccnet.com.cn/chain/proto/swarm"
	serviceswarm "gykjgit.dccnet.com.cn/chain/proto/swarm/service"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
	"net/http"
)

func RouteDir(hs *grope.GHttpServe) {
	// 仓库相关路由设置
	route := hs.Group("/dir")
	route.Post("/sync", sync)
}

func sync(ctx *grope.Context) {
	var (
		dir = &proto.ReqDir{}
		cid string
		err error
	)
	if err = ctx.ReceiveJSON(dir); nil != err {
		responseFailJSON(ctx, err)
		return
	}
	if cid, err = client.Obtain().Resource().ContainerID(dir.Server); nil != err {
		responseFailJSON(ctx, err)
		return
	}
	var cid12 = gnomon.SubString(cid, 0, 12)
	log.Warn("sync", log.Field("containerID", cid12))
	if _, err = serviceswarm.DirSync(gnomon.StringBuild(cid12, ":20219"), dir); nil != err {
		responseFailJSON(ctx, err)
		log.Warn("sync", log.Err(err))
		return
	}
	_ = ctx.ResponseText(http.StatusOK, "ok")
}
