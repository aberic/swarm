package router

import (
	"github.com/aberic/gnomon/grope"
	"github.com/aberic/proc"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
	"net/http"
)

func RouteProc(hs *grope.GHttpServe) {
	// 仓库相关路由设置
	route := hs.Group("/proc")
	route.Post("/listen", listen)
	route.Get("/resources", resources)
}

func listen(ctx *grope.Context) {
	p := &proc.Proc{}
	if err := ctx.ReceiveJSON(p); nil != err {
		responseFailJSON(ctx, err)
		return
	}
	client.Obtain().Proc().Listen(p)
	_ = ctx.ResponseText(http.StatusOK, "ok")
}

func resources(ctx *grope.Context) {
	_ = ctx.ResponseJSON(http.StatusOK, &response{Code: CodeSuccess, Data: client.Obtain().Proc().Resources()})
}
