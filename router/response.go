package router

import (
	"github.com/aberic/gnomon/grope"
	"net/http"
)

const (
	CodeSuccess = iota
	CodeFail
)

type response struct {
	Code   int
	ErrMsg string
	Data   interface{}
}

func responseFailJSON(ctx *grope.Context, err error) {
	_ = ctx.ResponseJSON(http.StatusOK, &response{Code: CodeFail, ErrMsg: err.Error()})
}
