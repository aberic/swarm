package rpc

import (
	"context"
	"github.com/aberic/gnomon/log"
	proto "gykjgit.dccnet.com.cn/chain/proto/swarm"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
)

type ResourceServer struct {
}

func (r *ResourceServer) Info(ctx context.Context, _ *proto.Request) (*proto.ResourceInfo, error) {
	return client.Obtain().Resource().Info(ctx)
}

func (r *ResourceServer) ContainersSync(ctx context.Context, req *proto.ReqContainerSync) (*proto.Response, error) {
	log.Debug("ContainersSync", log.Field("containers", req.Containers))
	return client.Obtain().Resource().SyncContainers(ctx, req)
}
