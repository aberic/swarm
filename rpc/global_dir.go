package rpc

import (
	"context"
	"github.com/aberic/gnomon"
	"github.com/aberic/gnomon/log"
	proto "gykjgit.dccnet.com.cn/chain/proto/swarm"
	serviceswarm "gykjgit.dccnet.com.cn/chain/proto/swarm/service"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
)

type GlobalDirServer struct {
}

func (d *GlobalDirServer) Sync(_ context.Context, dir *proto.ReqDir) (resp *proto.Response, err error) {
	log.Warn("GlobalDirServer sync receive ", log.Field("dir", dir))
	var cid string
	if cid, err = client.Obtain().Resource().ContainerID(dir.Server); nil != err {
		return &proto.Response{Code: proto.Code_fail, ErrMsg: err.Error()}, err
	}
	log.Warn("GlobalDirServer sync receive get cid", log.Field("cid", cid))
	var cid12 = gnomon.SubString(cid, 0, 12)
	log.Warn("sync", log.Field("containerID", cid12))
	if resp, err = serviceswarm.DirSync(gnomon.StringBuild(cid12, ":20219"), dir); nil != err {
		log.Warn("sync", log.Err(err))
		return
	}
	return
}
