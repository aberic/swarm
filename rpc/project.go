package rpc

import (
	"context"
	proto "gykjgit.dccnet.com.cn/chain/proto/swarm"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
)

// System 区块链中系统相关操作服务
type ProjectServer struct {
}

// Deploy 部署新的堆栈或更新现有堆栈
//
// tail 代表是否实时输出命令执行的日志信息
func (p *ProjectServer) Deploy(ctx context.Context, in *proto.ReqProject) (*proto.Response, error) {
	if err := client.Obtain().Project().Deploy(ctx, in); nil != err {
		return &proto.Response{Code: proto.Code_fail, ErrMsg: err.Error()}, err
	}
	return &proto.Response{Code: proto.Code_success}, nil
}

func (p *ProjectServer) Update(ctx context.Context, in *proto.ReqProjectUpdate) (*proto.Response, error) {
	if err := client.Obtain().Project().Update(ctx, in); nil != err {
		return &proto.Response{Code: proto.Code_fail, ErrMsg: err.Error()}, err
	}
	return &proto.Response{Code: proto.Code_success}, nil
}

func (p *ProjectServer) Remove(ctx context.Context, in *proto.ReqProjectRemove) (*proto.Response, error) {
	if err := client.Obtain().Project().Remove(ctx, in); nil != err {
		return &proto.Response{Code: proto.Code_fail, ErrMsg: err.Error()}, err
	}
	return &proto.Response{Code: proto.Code_success}, nil
}

func (p *ProjectServer) ServiceState(ctx context.Context, in *proto.ServiceCompare) (*proto.ServiceCompare, error) {
	return client.Obtain().Project().ServiceCompare(ctx, in)
}

func (p *ProjectServer) DeployContainer(ctx context.Context, in *proto.ReqProjectContainer) (*proto.Response, error) {
	if err := client.Obtain().Project().DeployContainer(ctx, in); nil != err {
		return &proto.Response{Code: proto.Code_fail, ErrMsg: err.Error()}, err
	}
	return &proto.Response{Code: proto.Code_success}, nil
}
