package rpc

import (
	"context"
	"encoding/json"
	proto "gykjgit.dccnet.com.cn/chain/proto/swarm"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
)

type ProcServer struct {
}

func (p *ProcServer) Resources(_ context.Context, _ *proto.ReqResources) (*proto.RespResources, error) {
	data, err := json.Marshal(client.Obtain().Proc().Resources())
	if nil != err {
		return &proto.RespResources{Code: proto.Code_fail, ErrMsg: err.Error()}, err
	}
	return &proto.RespResources{Code: proto.Code_success, Resources: data}, nil
}
