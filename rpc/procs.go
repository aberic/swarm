package rpc

import (
	"context"
	"encoding/json"
	"github.com/aberic/gnomon/log"
	"github.com/aberic/proc"
	"github.com/aberic/proc/protos"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
)

type ProcsServer struct {
}

func (p *ProcsServer) Info(_ context.Context, in *protos.Request) (*protos.Response, error) {
	pc := &proc.Proc{}
	err := json.Unmarshal(in.Proc, pc)
	log.Debug("ProcsServer", log.Field("procs", pc))
	if nil != err {
		return &protos.Response{Code: protos.Code_fail, ErrMsg: err.Error()}, err
	}
	client.Obtain().Proc().Listen(pc)
	return &protos.Response{Code: protos.Code_success}, nil
}
