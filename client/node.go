package client

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/swarm"
)

type Node struct {
}

func newNode() *Node {
	return &Node{}
}

// List 获取节点信息
func (n *Node) List(ctx context.Context) ([]swarm.Node, error) {
	return ObtainClient().NodeList(ctx, types.NodeListOptions{})
}

// Inspect 获取节点信息
func (n *Node) Inspect(ctx context.Context, nodeID string) (swarm.Node, []byte, error) {
	return ObtainClient().NodeInspectWithRaw(ctx, nodeID)
}
