package client

import (
	"context"
	"github.com/docker/docker/api/types"
	"io"
	"io/ioutil"
)

// Image Image
type Image struct {
}

func newImage() *Image {
	return &Image{}
}

// List List
func (i *Image) List(ctx context.Context) ([]types.ImageSummary, error) {
	return ObtainClient().ImageList(ctx, types.ImageListOptions{})
}

// Pull Pull
func (i *Image) Pull(ctx context.Context, refStr string) error { // "docker.io/library/hello-world:latest
	r, err := ObtainClient().ImagePull(ctx, refStr, types.ImagePullOptions{})
	if nil != err {
		return err
	}
	if _, err = io.Copy(ioutil.Discard, r); nil != err {
		return err
	}
	return nil
}

// Remove Remove
func (i *Image) Remove(ctx context.Context, id string) ([]types.ImageDeleteResponseItem, error) {
	return ObtainClient().ImageRemove(ctx, id, types.ImageRemoveOptions{})
}

// Tag Tag
func (i *Image) Tag(ctx context.Context, source, target string) error { // "busybox:latest", "busybox:test"
	return ObtainClient().ImageTag(ctx, source, target)
}
