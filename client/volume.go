package client

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/volume"
)

// Volume Volume
type Volume struct {
}

func newVolume() *Volume {
	return &Volume{}
}

// Create Create a network
func (v *Volume) Create(ctx context.Context, options volume.VolumeCreateBody) (types.Volume, error) {
	return ObtainClient().VolumeCreate(ctx, options)
}

// Remove Remove one or more networks
func (v *Volume) Remove(ctx context.Context, volumeID string, force bool) error {
	return ObtainClient().VolumeRemove(ctx, volumeID, force)
}

// Inspect Display detailed information on one or more networks
func (v *Volume) Inspect(ctx context.Context, volumeID string) (types.Volume, []byte, error) {
	return ObtainClient().VolumeInspectWithRaw(ctx, volumeID)
}

// List List networks
func (v *Volume) List(ctx context.Context) (volume.VolumeListOKBody, error) {
	return ObtainClient().VolumeList(ctx, filters.NewArgs())
}

// Prune Remove all unused networks
func (v *Volume) Prune(ctx context.Context, pruneFilters filters.Args) (types.VolumesPruneReport, error) {
	return ObtainClient().VolumesPrune(ctx, pruneFilters)
}
