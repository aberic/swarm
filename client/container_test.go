package client

import (
	"context"
	"encoding/json"
	"gotest.tools/assert"
	"testing"
	"time"
)

func TestContainer_Logs(t *testing.T) {
	since, err := time.Parse("2006/01/02 15:04:05", "2020/05/18 03:28:52")
	assert.NilError(t, err)
	until, err := time.Parse("2006/01/02 15:04:05", "2020/05/18 03:32:00")
	assert.NilError(t, err)
	bs, err := newContainer().Logs(context.Background(), "1dc579bc4921", since, until)
	assert.NilError(t, err)
	t.Log(string(bs))
}

func TestContainer_Inspect(t *testing.T) {
	c, bs, err := newContainer().Inspect(context.Background(), "57776ffd0812")
	assert.NilError(t, err)
	t.Log(string(bs))
	t.Log(c)
}

func TestContainer_List(t *testing.T) {
	cs, err := newContainer().List(context.Background())
	assert.NilError(t, err)
	data, err := json.Marshal(cs)
	assert.NilError(t, err)
	t.Log(string(data))
	for _, c := range cs {
		t.Log(c)
	}
}

func TestContainer_Stats(t *testing.T) {
	stat, osType, err := newContainer().Stats(context.Background(), "3de0ea8d22db")
	assert.NilError(t, err)
	t.Log(osType)
	bs, err := json.Marshal(stat)
	assert.NilError(t, err)
	t.Log(string(bs))
}

func TestContainer_StatsAll(t *testing.T) {
	stats, err := newContainer().StatsAll(context.Background())
	assert.NilError(t, err)
	data, err := json.Marshal(stats)
	assert.NilError(t, err)
	t.Log(string(data))
	for _, stat := range stats {
		t.Log(stat)
	}
}
