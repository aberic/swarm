package client

import (
	"context"
	"encoding/json"
	"github.com/docker/docker/api/types/filters"
	"gotest.tools/assert"
	"testing"
	"time"
)

func TestService_Logs(t *testing.T) {
	since, err := time.Parse("2006/01/02 15:04:05", "2020/05/18 03:28:52")
	assert.NilError(t, err)
	until, err := time.Parse("2006/01/02 15:04:05", "2020/05/18 03:32:00")
	assert.NilError(t, err)
	bs, err := newService().Logs(context.Background(), "i8kwvckghdqn", since, until)
	assert.NilError(t, err)
	t.Log(string(bs))
}

func TestService_Inspect(t *testing.T) {
	s, bs, err := newService().Inspect(context.Background(), "jtnnkdi40nsy")
	assert.NilError(t, err)
	t.Log(string(bs))
	t.Log(s)
}

func TestService_List(t *testing.T) {
	ss, err := newService().List(context.Background())
	assert.NilError(t, err)
	data, err := json.Marshal(ss)
	assert.NilError(t, err)
	t.Log(string(data))
	for _, s := range ss {
		t.Log(s)
	}
}

func TestService_Process(t *testing.T) {
	tasks, err := newService().Process(context.Background(), filters.NewArgs(filters.Arg("name", "my_nginx")))
	assert.NilError(t, err)
	for _, task := range tasks {
		t.Log(task)
	}
}

func TestService_Remove(t *testing.T) {
	t.Log(newService().Remove(context.Background(), "nginx"))
}

func TestService_ResourcesAll(t *testing.T) {
	r, err := newService().ResourcesAll(context.Background())
	assert.NilError(t, err)
	t.Log(r)
}

func TestService_Resources(t *testing.T) {
	r, err := newService().Resources(context.Background(), "jtnnkdi40nsy")
	assert.NilError(t, err)
	t.Log(r)
}

func TestService_GetServiceInfoByServiceName(t *testing.T) {
	// r, err := newService().GetServiceInfoByServiceName(context.Background(),"feifei014_ca")
	// assert.NilError(t, err)
	// t.Log(r)
}