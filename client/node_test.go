package client

import (
	"context"
	"encoding/json"
	"gotest.tools/assert"
	"testing"
)

func TestNode_List(t *testing.T) {
	ns, err := Obtain().Node().List(context.Background())
	assert.NilError(t, err)
	data, err := json.Marshal(ns)
	assert.NilError(t, err)
	t.Log(string(data))
}

func TestNode_Inspect(t *testing.T) {
	n, data, err := Obtain().Node().Inspect(context.Background(), "kaatxwrxdc1u0dcut5ij88jqp")
	assert.NilError(t, err)
	t.Log(n)
	t.Log(string(data))
}
