package client

import (
	"context"
	"testing"
)

func TestImage_List(t *testing.T) {
	if is, err := newImage().List(context.Background()); nil != err {
		t.Skip(err)
	} else {
		for _, i := range is {
			for _, tag := range i.RepoTags {
				t.Log(tag)
			}
			t.Log(i)
		}
	}
}
