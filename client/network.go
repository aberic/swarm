package client

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/network"
)

// Network Network
type Network struct {
}

func newNetwork() *Network {
	return &Network{}
}

// Create Create a network
func (s *Network) Create(ctx context.Context, name string, options types.NetworkCreate) (types.NetworkCreateResponse, error) {
	options.CheckDuplicate = true
	return ObtainClient().NetworkCreate(ctx, name, options)
}

// Remove Remove one or more networks
func (s *Network) Remove(ctx context.Context, networkID string) error {
	return ObtainClient().NetworkRemove(ctx, networkID)
}

// Inspect Display detailed information on one or more networks
func (s *Network) Inspect(ctx context.Context, networkID string, options types.NetworkInspectOptions) (types.NetworkResource, []byte, error) {
	return ObtainClient().NetworkInspectWithRaw(ctx, networkID, options)
}

// List List networks
func (s *Network) List(ctx context.Context, options types.NetworkListOptions) ([]types.NetworkResource, error) {
	return ObtainClient().NetworkList(ctx, options)
}

// Connect Connect a container to a network
func (s *Network) Connect(ctx context.Context, networkID, containerID string, config *network.EndpointSettings) error {
	return ObtainClient().NetworkConnect(ctx, networkID, containerID, config)
}

// Disconnect Disconnect a container from a network
func (s *Network) Disconnect(ctx context.Context, networkID, containerID string, force bool) error {
	return ObtainClient().NetworkDisconnect(ctx, networkID, containerID, force)
}

// Prune Remove all unused networks
func (s *Network) Prune(ctx context.Context, pruneFilters filters.Args) (types.NetworksPruneReport, error) {
	return ObtainClient().NetworksPrune(ctx, pruneFilters)
}
