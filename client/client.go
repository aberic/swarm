package client

import (
	"sync"
)

var (
	instance *Client
	once     sync.Once
)

// Client Client
type Client struct {
	proc      *Proc
	resource  *Resource
	docker    *Docker
	swarm     *Swarm
	node      *Node
	stack     *Stack
	project   *Project
	network   *Network
	service   *Service
	container *Container
	image     *Image
	volume    *Volume
}

// Obtain Obtain
func Obtain() *Client {
	once.Do(func() {
		if nil == instance {
			instance = newClient()
		}
	})
	return instance
}

func newClient() *Client {
	return &Client{
		proc:      newProc(),
		resource:  newResource(),
		docker:    newDocker(),
		swarm:     newSwarm(),
		node:      newNode(),
		stack:     newStack(),
		project:   newProject(),
		network:   newNetwork(),
		service:   newService(),
		container: newContainer(),
		image:     newImage(),
		volume:    newVolume(),
	}
}

// Proc Proc
func (c *Client) Proc() *Proc {
	return c.proc
}

// Resource Resource
func (c *Client) Resource() *Resource {
	return c.resource
}

// Docker Docker
func (c *Client) Docker() *Docker {
	return c.docker
}

// Swarm Swarm
func (c *Client) Swarm() *Swarm {
	return c.swarm
}

// Node Node
func (c *Client) Node() *Node {
	return c.node
}

// Stack 获取swarm堆栈服务对象
func (c *Client) Stack() *Stack {
	return c.stack
}

// Project 获取swarm堆栈服务对象
func (c *Client) Project() *Project {
	return c.project
}

// Network Network
func (c *Client) Network() *Network {
	return c.network
}

// Service Service
func (c *Client) Service() *Service {
	return c.service
}

// Container Container
func (c *Client) Container() *Container {
	return c.container
}

// Image Image
func (c *Client) Image() *Image {
	return c.image
}

// Volume Volume
func (c *Client) Volume() *Volume {
	return c.volume
}
