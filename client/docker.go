package client

import (
	"context"
	"github.com/docker/docker/api/types"
)

type Docker struct {
}

func newDocker() *Docker {
	return &Docker{}
}

// Info 获取资源信息
func (d *Docker) Info(ctx context.Context) (info types.Info, err error) {
	return ObtainClient().Info(ctx)
}
