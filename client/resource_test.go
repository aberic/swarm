package client

import (
	"context"
	"encoding/json"
	"gotest.tools/assert"
	"testing"
)

func TestResource_Info(t *testing.T) {
	r, err := Obtain().Resource().Info(context.Background())
	assert.NilError(t, err)
	data, err := json.Marshal(r)
	assert.NilError(t, err)
	t.Log(string(data))
}
