package client

import (
	"context"
	"encoding/json"
	"gotest.tools/assert"
	"testing"
)

func TestDocker_Info(t *testing.T) {
	info, err := Obtain().Docker().Info(context.Background())
	assert.NilError(t, err)
	data, err := json.Marshal(info)
	assert.NilError(t, err)
	t.Log(string(data))
}
