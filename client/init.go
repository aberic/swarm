package client

import (
	"github.com/docker/docker/client"
	"sync"
)

var (
	clt     *client.Client
	cltOnce sync.Once
)

func ObtainClient() *client.Client {
	cltOnce.Do(func() {
		var err error
		if clt, err = clientCreate(); nil != err {
			panic(err)
		}
	})
	return clt
}

// clientCreate
//
// FromEnv configures the client with values from environment variables.
//
// Supported environment variables:
// DOCKER_HOST to set the url to the docker server.
// DOCKER_API_VERSION to set the version of the API to reach, leave empty for latest.
// DOCKER_CERT_PATH to load the TLS certificates from.
// DOCKER_TLS_VERIFY to enable or disable TLS verification, off by default.
func clientCreate() (*client.Client, error) {
	return client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
}
