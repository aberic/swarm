package client

import (
	"encoding/json"
	"github.com/docker/docker/api/types/swarm"
	"gotest.tools/assert"
	"testing"
)

func TestSwarm_Init(t *testing.T) {
	//res, err := Obtain().swarm.Init(swarm.InitRequest{})
	res, err := Obtain().swarm.Init(swarm.InitRequest{ForceNewCluster: true})
	assert.NilError(t, err)
	t.Log(res)
}

func TestSwarm_Inspect(t *testing.T) {
	info, err := newSwarm().Inspect()
	assert.NilError(t, err)
	data, err := json.Marshal(info)
	assert.NilError(t, err)
	t.Log(string(data))
}
