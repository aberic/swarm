package client

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	proto "gykjgit.dccnet.com.cn/chain/proto/swarm"
)

func TestProject_Deploy(t *testing.T) {
	t.Log(newProject().Deploy(context.Background(), &proto.ReqProject{
		Name: "nginx",
		Services: map[string]*proto.Task{
			"nginx": {
				Image:        &proto.Image{Name: "nginx", Version: "1.18.0"},
				Ports:        []*proto.Port{{Publish: "8088", Target: "80"}},
				Environments: []*proto.Environment{{Key: "KEY", Value: "value"}},
				Volumes:      []*proto.ComposeVolume{{Local: "/Users/aberic/Documents/path/go/src/gykjgit.dccnet.com.cn/chain/baas/work/yml/job/yapi/docker", Mount: "/home"}},
				Networks:     []string{"nginx"},
				TTY:          true,
				ExtraHosts:   []*proto.ExtraHost{{Host: "orderer0.hnachain.com", HostMap: "172.17.0.1"}},
				Deploy: &proto.Deploy{
					Mode:         proto.Mode_replicated,
					Replicas:     3,
					EndpointMode: proto.EndpointMode_vip,
					Labels: map[string]string{
						"com.example.description1": "Accounting webapp",
						"com.example.description2": "Accounting webapp",
					},
					Placement: &proto.Placement{
						Constraints: []string{"node.role == manager"},
						Preferences: []*proto.Preference{
							{Spread: "node.labels.zone"},
						},
					},
					RestartPolicy: &proto.RestartPolicy{
						Condition:   proto.Condition_onFailure,
						Delay:       "5s",
						MaxAttempts: 3,
						Window:      "120s",
					},
					UpdateConfig: &proto.UpdateConfig{
						Parallelism:     2,
						Delay:           "10s",
						FailureAction:   proto.Action_rollback,
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           proto.Order_stopFirst,
					},
					RollbackConfig: &proto.UpdateConfig{
						Parallelism:     2,
						Delay:           "10s",
						FailureAction:   proto.Action_continue,
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           proto.Order_startFirst,
					},
				},
			},
		},
	}))
}

func TestProject_ServiceStats(t *testing.T) {
	list, err := newProject().ServiceStats(context.Background(), "test_", false)
	if list != nil {
		str, err := json.Marshal(list)
		if err != nil {
			t.Log("json Marshal Error: ", err.Error())
		} else {
			t.Log(string(str))
		}
	} else if err != nil {
		t.Log("List is nil, Error: ", err.Error())
	} else {
		t.Log("List is nil")
	}
}

func TestProject_ServiceCompare(t *testing.T) {
	p := Project{}
	cc, err := p.ServiceCompare(context.Background(), &proto.ServiceCompare{
		Infos: []*proto.ServiceCompareInfo{
			{
				Name: "tender_jackson",
			},
		},
	})
	if err != nil {
		panic(err)
	}

	fmt.Println(cc)
}
