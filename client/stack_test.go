package client

import (
	"gykjgit.dccnet.com.cn/chain/swarm/model"
	"testing"
)

func TestStack_Deploy(t *testing.T) {
	t.Log(newStack().Deploy(&model.Compose{
		Version: "3.7",
		Services: map[string]*model.Task{
			"nginx": {
				Image:      "nginx:1.18.0",
				Ports:      []string{"8080:80"},
				Networks:   []string{"chain"},
				Volumes:    []string{"/Users/aberic/Documents/path/go/src/gykjgit.dccnet.com.cn/chain/baas/work/yml/job/yapi/docker:/home"},
				ExtraHosts: []string{"orderer0.hnachain.com:172.17.0.1"},
				Deploy: &model.Deploy{
					Mode:         "replicated",
					Replicas:     3,
					EndpointMode: "vip",
					//Resources: &model.Resources{
					//	Limits:       &model.Resource{CPUs: "1", Memory: "400M"},
					//	Reservations: &model.Resource{CPUs: "0.5", Memory: "200M"},
					//},
					Labels: map[string]string{
						"com.example.description1": "Accounting webapp",
						"com.example.description2": "Accounting webapp",
					},
					Placement: &model.Placement{
						Constraints: []string{"node.role == manager"},
						Preferences: []*model.Preference{
							{Spread: "node.labels.zone"},
						},
					},
					RestartPolicy: &model.RestartPolicy{
						Condition:   "on-failure",
						Delay:       "5s",
						MaxAttempts: 3,
						Window:      "120s",
					},
					UpdateConfig: &model.UpdateConfig{
						Parallelism:     2,
						Delay:           "10s",
						FailureAction:   "rollback",
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           "stop-first",
					},
					RollbackConfig: &model.RollbackConfig{
						Parallelism:     2,
						Delay:           "10s",
						FailureAction:   "continue",
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           "stop-first",
					},
				},
			},
		},
		Networks: map[string]*model.Network{
			"chain": {
				Driver: "overlay",
			},
		},
	}, "nginx"))
}

func TestStack_List(t *testing.T) {
	if stacks, err := newStack().List(); nil != err {
		t.Error(err)
	} else {
		for _, stack := range stacks {
			t.Log(stack)
		}
	}
}

func TestStack_ProcessEmpty(t *testing.T) {
	if sps, err := newStack().Process("protainer"); nil != err {
		t.Log(err)
	} else {
		t.Log(sps)
	}
}

func TestStack_Process(t *testing.T) {
	if sps, err := newStack().Process("nginx"); nil != err {
		t.Error(err)
	} else {
		for _, sp := range sps {
			t.Log(sp)
		}
	}
}

func TestStack_ServicesEmpty(t *testing.T) {
	if services, err := newStack().Services("portainer1"); nil != err {
		t.Log(err)
	} else {
		t.Log(services)
	}
}

func TestStack_Services(t *testing.T) {
	if services, err := newStack().Services("nginx"); nil != err {
		t.Error(err)
	} else {
		for _, s := range services {
			t.Log(s)
			for _, p := range s.Ports {
				t.Log(p)
			}
		}
	}
}

func TestStack_RemoveEmpty(t *testing.T) {
	t.Log(newStack().Remove("portainer1"))
}

func TestStack_Remove(t *testing.T) {
	t.Log(newStack().Remove("nginx"))
}
