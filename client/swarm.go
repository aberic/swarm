package client

import (
	"context"
	"github.com/aberic/gnomon"
	"github.com/docker/docker/api/types/swarm"
)

type Swarm struct {
}

func newSwarm() *Swarm {
	return &Swarm{}
}

// Init Init the swarm.
func (s *Swarm) Init(req swarm.InitRequest) (string, error) {
	if gnomon.StringIsEmpty(req.ListenAddr) {
		req.ListenAddr = "0.0.0.0:2377"
	}
	return ObtainClient().SwarmInit(context.Background(), req)
}

// Join Join the swarm.
func (s *Swarm) Join(req swarm.JoinRequest) error {
	return ObtainClient().SwarmJoin(context.Background(), req)
}

// Inspect inspects the swarm.
func (s *Swarm) Inspect() (swarm.Swarm, error) {
	return ObtainClient().SwarmInspect(context.Background())
}
