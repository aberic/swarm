package client

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"gotest.tools/assert"
	"testing"
)

func TestNetwork_Create(t *testing.T) {
	resp, err := Obtain().network.Create(context.Background(), "test", types.NetworkCreate{
		Driver:     "overlay",
		Scope:      "swarm",
		Attachable: true,
	})
	assert.NilError(t, err)
	t.Log(resp)
}

func TestNetwork_Inspect(t *testing.T) {
	resp, bs, err := Obtain().network.Inspect(context.Background(), "test", types.NetworkInspectOptions{})
	assert.NilError(t, err)
	t.Log(string(bs))
	t.Log(resp)
}

func TestNetwork_List(t *testing.T) {
	resp, err := Obtain().network.List(context.Background(), types.NetworkListOptions{})
	assert.NilError(t, err)
	for _, r := range resp {
		t.Log(r)
	}
}

func TestNetwork_Remove(t *testing.T) {
	err := Obtain().network.Remove(context.Background(), "test")
	assert.NilError(t, err)
}

func TestNetwork_Prune(t *testing.T) {
	resp, err := Obtain().network.Prune(context.Background(), filters.NewArgs())
	assert.NilError(t, err)
	t.Log(resp)
}
