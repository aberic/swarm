package client

import (
	"github.com/aberic/proc"
	"sync"
)

// Proc Proc
type Proc struct {
	ps map[string]*proc.Proc
	mu sync.RWMutex
}

func newProc() *Proc {
	return &Proc{ps: map[string]*proc.Proc{}}
}

// Listen Listen
func (p *Proc) Listen(pNew *proc.Proc) {
	defer p.mu.Unlock()
	p.mu.Lock()
	p.ps[pNew.Hostname] = pNew
}

// Resources Resources
func (p *Proc) Resources() map[string]*proc.Proc {
	return p.ps
}
