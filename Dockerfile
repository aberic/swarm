FROM golang:1.14.3 as builder
LABEL app="swarm" by="aberic"
ENV GOPROXY=https://goproxy.io
ENV GO111MODULE=on
ENV REPO=$GOPATH/src/gykjgit.dccnet.com.cn/chain/swarm
WORKDIR $REPO
ADD . $REPO
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build
FROM docker.io/centos:7
RUN mkdir -p /home/data/log && \
    mkdir -p /home/data/platform
ENV WORK_PATH=/home
WORKDIR $WORK_PATH
COPY --from=builder /go/src/gykjgit.dccnet.com.cn/chain/swarm/swarm .
EXPOSE 20219
EXPOSE 20220
CMD ./swarm