package model

// ImagePull ImagePull
type ImagePull struct {
	RefStr string `json:"refStr"`
}

// ImageTag ImageTag
type ImageTag struct {
	Source string `json:"source"`
	Target string `json:"target"`
}
