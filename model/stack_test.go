package model

import (
	"encoding/json"
	"gotest.tools/assert"
	proto "gykjgit.dccnet.com.cn/chain/proto/swarm"
	"testing"
)

var protoCompose = &proto.Compose{
	Services: map[string]*proto.Task{
		"consul": {
			Image: &proto.Image{
				Name:    "consul",
				Version: "1.4.4",
			},
			Ports: []*proto.Port{
				{Publish: "8080", Target: "8090"},
				{Publish: "8050", Target: "8060"},
			},
			Environments: []*proto.Environment{
				{Key: "CONSUL_BIND_INTERFACE", Value: "eth0"},
				{Key: "CONSUL_BIND_INTERFACE", Value: "eth1"},
			},
			Command:    []string{"agent"},
			Args:       []string{" -server -bootstrap -ui -node=1 -client='0.0.0.0'"},
			WorkingDir: "/opt/gopath/src/github.com/hyperledger/fabric/peer",
			Volumes: []*proto.ComposeVolume{
				{Local: "/data/mount/dev/consuls/consul1", Mount: "/consul/data"},
				{Local: "/data/mount/dev/consuls/consul1", Mount: "/consul/data1"},
			},
			Networks: []string{"chain"},
			TTY:      true,
			ExtraHosts: []*proto.ExtraHost{
				{Host: "peer0.org1.hnachain.com", HostMap: "172.17.0.1"},
				{Host: "peer0.org1.hnachain.com", HostMap: "172.17.0.1"},
			},
			DependsOn: []string{"db", "redis"},
			Deploy: &proto.Deploy{
				Mode:         0,
				Replicas:     3,
				EndpointMode: 0,
				Resources: &proto.Resources{
					Limits: &proto.Resource{
						Cpus:   "0.5",
						Memory: "400M",
					},
					Reservations: &proto.Resource{
						Cpus:   "2",
						Memory: "800M",
					},
				},
				Labels: map[string]string{
					"com.example.description1": "Accounting webapp",
					"com.example.description2": "Accounting webapp",
				},
				Placement: &proto.Placement{
					Constraints: []string{"node.role == manager", "engine.labels.operatingsystem == ubuntu 14.04"},
					Preferences: []*proto.Preference{{Spread: "node.labels.zone"}},
				},
				RestartPolicy: &proto.RestartPolicy{
					Condition:   0,
					Delay:       "3s",
					MaxAttempts: 3,
					Window:      "120s",
				},
				UpdateConfig: &proto.UpdateConfig{
					Parallelism:     3,
					Delay:           "5s",
					FailureAction:   0,
					Monitor:         "3s",
					MaxFailureRatio: 0.1,
					Order:           1,
				},
				RollbackConfig: &proto.UpdateConfig{
					Parallelism:     3,
					Delay:           "5s",
					FailureAction:   0,
					Monitor:         "3s",
					MaxFailureRatio: 0.1,
					Order:           1,
				},
			},
		},
	},
	Networks: map[string]*proto.ComposeNetwork{
		"chain": {Driver: proto.Driver_overlay},
	},
}

func TestNewCompose(t *testing.T) {
	bs, err := json.Marshal(protoCompose)
	assert.NilError(t, err)
	t.Log(string(bs))
	c, err := NewCompose(protoCompose)
	assert.NilError(t, err)
	t.Log(c.WriteTo("tmp/d.yaml"))
}

func TestNewComposeMulti(t *testing.T) {
	c, err := NewCompose(&proto.Compose{
		Services: map[string]*proto.Task{
			"consul": {
				Image: &proto.Image{
					Name:    "consul",
					Version: "1.4.4",
				},
				Ports: []*proto.Port{
					{Publish: "8080", Target: "8090"},
					{Publish: "8050", Target: "8060"},
				},
				Environments: []*proto.Environment{
					{Key: "CONSUL_BIND_INTERFACE", Value: "eth0"},
					{Key: "CONSUL_BIND_INTERFACE", Value: "eth1"},
				},
				Command:    []string{"agent"},
				Args:       []string{" -server -bootstrap -ui -node=1 -client='0.0.0.0'"},
				WorkingDir: "/opt/gopath/src/github.com/hyperledger/fabric/peer",
				Volumes: []*proto.ComposeVolume{
					{Local: "/data/mount/dev/consuls/consul1", Mount: "/consul/data"},
					{Local: "/data/mount/dev/consuls/consul1", Mount: "/consul/data1"},
				},
				Networks: []string{"chain"},
				TTY:      true,
				ExtraHosts: []*proto.ExtraHost{
					{Host: "peer0.org1.hnachain.com", HostMap: "172.17.0.1"},
					{Host: "peer0.org1.hnachain.com", HostMap: "172.17.0.1"},
				},
				DependsOn: []string{"db", "redis"},
				Deploy: &proto.Deploy{
					Mode:         0,
					Replicas:     3,
					EndpointMode: 0,
					Resources: &proto.Resources{
						Limits: &proto.Resource{
							Cpus:   "0.5",
							Memory: "400M",
						},
						Reservations: &proto.Resource{
							Cpus:   "2",
							Memory: "800M",
						},
					},
					Labels: map[string]string{
						"com.example.description1": "Accounting webapp",
						"com.example.description2": "Accounting webapp",
					},
					Placement: &proto.Placement{
						Constraints: []string{"node.role == manager", "engine.labels.operatingsystem == ubuntu 14.04"},
						Preferences: []*proto.Preference{{Spread: "node.labels.zone"}},
					},
					RestartPolicy: &proto.RestartPolicy{
						Condition:   0,
						Delay:       "3s",
						MaxAttempts: 3,
						Window:      "120s",
					},
					UpdateConfig: &proto.UpdateConfig{
						Parallelism:     3,
						Delay:           "5s",
						FailureAction:   0,
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           1,
					},
					RollbackConfig: &proto.UpdateConfig{
						Parallelism:     3,
						Delay:           "5s",
						FailureAction:   0,
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           1,
					},
				},
			},
			"gateway": {
				Image: &proto.Image{
					Name:    "gateway",
					Version: "1.4.4",
				},
				Ports: []*proto.Port{
					{Publish: "8080", Target: "8090"},
				},
				Environments: []*proto.Environment{
					{Key: "CONSUL_BIND_INTERFACE", Value: "eth0"},
				},
				Volumes: []*proto.ComposeVolume{
					{Local: "/data/mount/dev/consuls/consul1", Mount: "/consul/data"},
				},
				Networks: []string{"chain"},
				TTY:      true,
				ExtraHosts: []*proto.ExtraHost{
					{Host: "peer0.org1.hnachain.com", HostMap: "172.17.0.1"},
				},
				Deploy: &proto.Deploy{
					Mode:         0,
					Replicas:     3,
					EndpointMode: 0,
					Resources: &proto.Resources{
						Limits: &proto.Resource{
							Cpus:   "0.5",
							Memory: "400M",
						},
						Reservations: &proto.Resource{
							Cpus:   "2",
							Memory: "800M",
						},
					},
					Labels: map[string]string{
						"com.example.description1": "Accounting webapp",
						"com.example.description2": "Accounting webapp",
					},
					Placement: &proto.Placement{
						Constraints: []string{"node.role == manager", "engine.labels.operatingsystem == ubuntu 14.04"},
						Preferences: []*proto.Preference{{Spread: "node.labels.zone"}},
					},
					RestartPolicy: &proto.RestartPolicy{
						Condition:   0,
						Delay:       "3s",
						MaxAttempts: 3,
						Window:      "120s",
					},
					UpdateConfig: &proto.UpdateConfig{
						Parallelism:     3,
						Delay:           "5s",
						FailureAction:   0,
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           1,
					},
					RollbackConfig: &proto.UpdateConfig{
						Parallelism:     3,
						Delay:           "5s",
						FailureAction:   0,
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           1,
					},
				},
			},
		},
		Networks: map[string]*proto.ComposeNetwork{
			"chain": {Driver: proto.Driver_overlay},
		},
	})
	assert.NilError(t, err)
	t.Log(c.WriteTo("tmp/e.yaml"))
}

func TestCompose_WriteTo(t *testing.T) {
	c := &Compose{
		Version: "3.8",
		Services: map[string]*Task{
			"consul": {
				Image:       "consul:1.4.4",
				Ports:       []string{"8500:8500", "8600:8600"},
				Environment: []string{"CONSUL_BIND_INTERFACE=eth0", "CONSUL_BIND_INTERFACE=eth1"},
				Commands:    []string{"agent"},
				Args:        []string{" -server -bootstrap -ui -node=1 -client='0.0.0.0'"},
				WorkingDir:  "/opt/gopath/src/github.com/hyperledger/fabric/peer",
				Volumes:     []string{"/data/mount/dev/consuls/consul1:/consul/data", "/data/mount/dev/consuls/consul1:/consul/data1"},
				Networks:    []string{"chain"},
				TTY:         true,
				ExtraHosts:  []string{"peer0.org1.hnachain.com:172.17.0.1"},
				DependsOn:   []string{"db", "redis"},
				Deploy: &Deploy{
					Mode:         "replicated",
					Replicas:     3,
					EndpointMode: "vip",
					Resources: &Resources{
						Limits:       &Resource{CPUs: "1", Memory: "400M"},
						Reservations: &Resource{CPUs: "0.5", Memory: "200M"},
					},
					Labels: map[string]string{
						"com.example.description1": "Accounting webapp",
						"com.example.description2": "Accounting webapp",
					},
					Placement: &Placement{
						Constraints: []string{"node.role == manager", "engine.labels.operatingsystem == ubuntu 14.04"},
						Preferences: []*Preference{
							{Spread: "node.labels.zone"},
						},
					},
					RestartPolicy: &RestartPolicy{
						Condition:   "on-failure",
						Delay:       "5s",
						MaxAttempts: 3,
						Window:      "120s",
					},
					UpdateConfig: &UpdateConfig{
						Parallelism:     2,
						Delay:           "10s",
						FailureAction:   "rollback",
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           "stop-first",
					},
					RollbackConfig: &RollbackConfig{
						Parallelism:     2,
						Delay:           "10s",
						FailureAction:   "continue",
						Monitor:         "3s",
						MaxFailureRatio: 0.1,
						Order:           "stop-first",
					},
				},
			},
		},
		Networks: map[string]*Network{
			"chain": {
				Driver: "overlay",
			},
		},
	}
	t.Log(c.WriteTo("tmp/c.yaml"))
}
