package rpcjson

import (
	"github.com/docker/docker/api/types"
	"gotest.tools/assert"
	"gykjgit.dccnet.com.cn/chain/swarm/rpcjson/model"
	"testing"
)

func TestImage_List(t *testing.T) {
	var (
		image = &Image{}
		resp  = &model.RespImageList{}
		err   error
	)
	err = image.List(model.ReqImageList{}, resp)
	assert.NilError(t, err)
	for _, i := range resp.Images {
		t.Log(i)
	}
}

func TestImage_Pull(t *testing.T) {
	var (
		image = &Image{}
		resp  = &model.RespImagePull{}
		err   error
	)
	err = image.Pull(model.ReqImagePull{
		RefStr:  "xiaoxijin/apline:latest",
		Options: types.ImagePullOptions{},
	}, resp)
	assert.NilError(t, err)
}
