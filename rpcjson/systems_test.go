package rpcjson

import (
	"github.com/docker/docker/api/types"
	"gotest.tools/assert"
	"gykjgit.dccnet.com.cn/chain/swarm/rpcjson/model"
	"testing"
)

func TestLogin_RegistryLogin(t *testing.T) {
	var (
		login = &System{}
		resp  = &model.RespSystemRegistryLogin{}
		err   error
	)
	err = login.RegistryLogin(model.ReqSystemRegistryLogin{Auth: types.AuthConfig{
		Username: "aberic",
		Password: "no-password",
	}}, resp)
	assert.NilError(t, err)
	t.Log(resp.Authenticate.IdentityToken)
	t.Log(resp.Authenticate.Status)
}
