package rpcjson

import (
	"context"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
	"gykjgit.dccnet.com.cn/chain/swarm/rpcjson/model"
)

// Volume Volume
type Volume struct{}

// Create Create a network
func (Volume) Create(req model.ReqVolumeCreate, resp *model.RespVolumeCreate) error {
	volume, err := client.ObtainClient().VolumeCreate(context.Background(), req.Options)
	if nil != err {
		return err
	}
	resp.Volume = volume
	return nil
}

// Remove Remove one or more networks
func (Volume) Remove(req model.ReqVolumeRemove, _ *model.RespVolumeRemove) error {
	return client.ObtainClient().VolumeRemove(context.Background(), req.VolumeID, req.Force)
}

// Inspect Display detailed information on one or more networks
func (Volume) Inspect(req model.ReqVolumeInspect, resp *model.RespVolumeInspect) error {
	v, data, err := client.ObtainClient().VolumeInspectWithRaw(context.Background(), req.VolumeID)
	if nil != err {
		return err
	}
	resp.Volume = v
	resp.Data = data
	return nil
}

// List List networks
func (Volume) List(req model.ReqVolumeList, resp *model.RespVolumeList) error {
	body, err := client.ObtainClient().VolumeList(context.Background(), req.Filter)
	if nil != err {
		return err
	}
	resp.Body = body
	return nil
}

// Prune Remove all unused networks
func (Volume) Prune(req model.ReqVolumePrune, resp *model.RespVolumePrune) error {
	report, err := client.ObtainClient().VolumesPrune(context.Background(), req.Filters)
	if nil != err {
		return err
	}
	resp.Report = report
	return nil
}
