package rpcjson

import (
	"context"
	"github.com/pkg/errors"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
	"gykjgit.dccnet.com.cn/chain/swarm/rpcjson/model"
)

type System struct{}

func (System) Info(_ model.ReqSystemInfo, resp *model.RespSystemInfo) error {
	info, err := client.ObtainClient().Info(context.Background())
	if nil != err {
		return errors.Wrap(err, "docker info error")
	}
	resp.Info = info
	return nil
}

func (System) RegistryLogin(req model.ReqSystemRegistryLogin, resp *model.RespSystemRegistryLogin) error {
	authenticate, err := client.ObtainClient().RegistryLogin(context.Background(), req.Auth)
	if nil != err {
		return err
	}
	resp.Authenticate = authenticate
	return nil
}

func (System) DiskUsage(_ model.ReqSystemDiskUsage, resp *model.RespSystemDiskUsage) error {
	diskUsage, err := client.ObtainClient().DiskUsage(context.Background())
	if nil != err {
		return err
	}
	resp.DiskUsage = diskUsage
	return nil
}

func (System) Ping(_ model.ReqSystemPing, resp *model.RespSystemPing) error {
	ping, err := client.ObtainClient().Ping(context.Background())
	if nil != err {
		return err
	}
	resp.Ping = ping
	return nil
}
