package rpcjson

import (
	"gotest.tools/assert"
	"gykjgit.dccnet.com.cn/chain/swarm/rpcjson/model"
	"testing"
	"time"
)

func TestContainersLogs(t *testing.T) {
	var (
		c            = &Container{}
		since, until time.Time
		resp         = &model.RespContainerLogs{}
		err          error
	)
	since, err = time.Parse("2006/01/02 15:04:05", "2020/08/11 01:24:53")
	assert.NilError(t, err)
	until, err = time.Parse("2006/01/02 15:04:05", "2020/08/11 02:43:50")
	assert.NilError(t, err)
	err = c.Logs(model.ReqContainerLogs{
		ContainerID: "5644efde1555",
		Timestamps:  false,
		Details:     false,
		Follow:      false,
		Tail:        "all",
		Since:       since,
		Until:       until,
	}, resp)
	assert.NilError(t, err)
	t.Log(string(resp.Data))
}
