package rpcjson

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/swarm"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
)

type Task struct{}

// List 获取服务信息
func (Task) List(req types.TaskListOptions, resp *[]swarm.Task) error {
	response, err := client.ObtainClient().TaskList(context.Background(), req)
	if nil != err {
		return err
	}
	resp = &response
	return nil
}
