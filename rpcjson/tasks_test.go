package rpcjson

import (
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
	"gotest.tools/assert"
	"testing"
)

func TestTask_List(t *testing.T) {
	var (
		task = &Task{}
		resp = &[]swarm.Task{}
		err  error
	)
	err = task.List(types.TaskListOptions{
		Filters: filters.NewArgs(filters.Arg("name", "my_nginx")),
	}, resp)
	assert.NilError(t, err)
	for _, i := range *resp {
		t.Log(i)
	}
}
