package rpcjson

import (
	"context"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
	"gykjgit.dccnet.com.cn/chain/swarm/rpcjson/model"
)

type Node struct{}

// List 获取节点信息
func (Node) List(req model.ReqNodeList, resp *model.RespNodeList) error {
	fil:=filters.NewArgs(req.Options...)
	nodes, err := client.ObtainClient().NodeList(context.Background(), types.NodeListOptions{Filters:fil})
	if nil != err {
		return err
	}
	resp.Nodes = nodes
	return nil
}

// Inspect 获取节点信息
func (Node) Inspect(req model.ReqNodeInspect, resp *model.RespNodeInspect) error {
	node, data, err := client.ObtainClient().NodeInspectWithRaw(context.Background(), req.NodeID)
	if nil != err {
		return err
	}
	resp.Node = node
	resp.Data = data
	return nil
}
