package model

import (
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
)

type ReqNodeList struct {
	// Options types.NodeListOptions
	Options []filters.KeyValuePair
}

type RespNodeList struct {
	Nodes []swarm.Node
}

type ReqNodeInspect struct {
	NodeID string
}

type RespNodeInspect struct {
	Node swarm.Node
	Data []byte
}
