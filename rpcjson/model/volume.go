package model

import (
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/volume"
)

type ReqVolumeCreate struct {
	Options volume.VolumeCreateBody
}

type RespVolumeCreate struct {
	Volume types.Volume
}

type ReqVolumeRemove struct {
	VolumeID string
	Force    bool
}

type RespVolumeRemove struct{}

type ReqVolumeInspect struct {
	VolumeID string
}

type RespVolumeInspect struct {
	Volume types.Volume
	Data   []byte
}

type ReqVolumeList struct {
	Filter filters.Args
}

type RespVolumeList struct {
	Body volume.VolumeListOKBody
}

type ReqVolumePrune struct {
	Filters filters.Args
}

type RespVolumePrune struct {
	Report types.VolumesPruneReport
}
