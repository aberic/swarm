package model

import "github.com/docker/docker/api/types"

type ReqImageList struct {
	Options types.ImageListOptions
}

type RespImageList struct {
	Images []types.ImageSummary
}

type ReqImagePull struct {
	RefStr  string
	Options types.ImagePullOptions
}

type RespImagePull struct{}

type ReqImageRemove struct {
	ID      string
	Options types.ImageRemoveOptions
}

type RespImageRemove struct {
	ImageItems []types.ImageDeleteResponseItem
}

type ReqImageTag struct { // "busybox:latest", "busybox:test"
	Source string
	Target string
}

type RespImageTag struct{}
