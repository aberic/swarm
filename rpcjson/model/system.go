package model

import (
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/registry"
)

type ReqSystemInfo struct{}

type RespSystemInfo struct {
	Info types.Info
}

type ReqSystemRegistryLogin struct {
	Auth types.AuthConfig
}

type RespSystemRegistryLogin struct {
	Authenticate registry.AuthenticateOKBody
}

type ReqSystemDiskUsage struct{}

type RespSystemDiskUsage struct {
	DiskUsage types.DiskUsage
}

type ReqSystemPing struct{}

type RespSystemPing struct {
	Ping types.Ping
}
