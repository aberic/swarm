package model

import (
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
)
import "github.com/docker/docker/api/types/network"

type ReqNetworkCreate struct {
	Name    string
	Options types.NetworkCreate
}

type RespNetworkCreate struct {
	Response types.NetworkCreateResponse
}

type ReqNetworkRemove struct {
	NetworkID string
}

type RespNetworkRemove struct{}

type ReqNetworkInspect struct {
	NetworkID string
	Options   types.NetworkInspectOptions
}

type RespNetworkInspect struct {
	Resource types.NetworkResource
	Data     []byte
}

type ReqNetworkList struct {
	Options types.NetworkListOptions
}

type RespNetworkList struct {
	Resource []types.NetworkResource
}

type ReqNetworkConnect struct {
	NetworkID   string
	ContainerID string
	Config      *network.EndpointSettings
}

type RespNetworkConnect struct{}

type ReqNetworkDisconnect struct {
	NetworkID   string
	ContainerID string
	Force       bool
}

type RespNetworkDisconnect struct{}

type ReqNetworkPrune struct {
	Filters filters.Args
}

type RespNetworkPrune struct {
	Report types.NetworksPruneReport
}
