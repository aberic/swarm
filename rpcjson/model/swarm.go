package model

import "github.com/docker/docker/api/types/swarm"

type ReqSwarmInit struct {
	Request swarm.InitRequest
}

type RespSwarmInit struct {
	Response string
}

type ReqSwarmJoin struct {
	Request swarm.JoinRequest
}

type RespSwarmJoin struct {
}

type ReqSwarmInspect struct {
}

type RespSwarmInspect struct {
	SwarmInfo swarm.Swarm
}
