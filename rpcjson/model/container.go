package model

import (
	"github.com/docker/docker/api/types"
	"time"
)

type ReqContainerLogs struct {
	ContainerID string
	Timestamps  bool   // 显示时间戳
	Details     bool   // 显示提供给日志的额外细节
	Follow      bool   // 跟踪日志输出
	Tail        string // 从日志末尾显示的行数(默认为“all”)
	Since       time.Time
	Until       time.Time
}

type RespContainerLogs struct {
	Data []byte
}

type ReqContainerList struct {
	Options types.ContainerListOptions
}

type RespContainerList struct {
	Containers []types.Container
}

type ReqContainerInspect struct {
	ContainerID string
	GetSize     bool
}

type RespContainerInspect struct {
	ContainerJSON types.ContainerJSON
	Data          []byte
}

type ReqContainerStats struct {
	ContainerID string
	Stream      bool
}

type RespContainerStats struct {
	Stats  types.Stats
	OSType string
}

type ReqContainerStatsAll struct {
	Options types.ContainerListOptions
}

type RespContainerStatsAll struct {
	StatsAll []RespContainerStats
}
