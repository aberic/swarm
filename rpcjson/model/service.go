package model

import (
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
	"time"
)

type ReqServiceCreate struct {
	Service swarm.ServiceSpec
	Options types.ServiceCreateOptions
}

type RespServiceCreate struct {
	Response types.ServiceCreateResponse
}

type ReqServiceLogs struct {
	ServiceID  string
	Timestamps bool   // 显示时间戳
	Details    bool   // 显示提供给日志的额外细节
	Follow     bool   // 跟踪日志输出
	Tail       string // 从日志末尾显示的行数(默认为“all”)
	Since      time.Time
	Until      time.Time
}

type RespServiceLogs struct {
	Data []byte
}

type ReqServiceList struct {
	Options types.ServiceListOptions
}

type RespServiceList struct {
	Services []swarm.Service
}

type ReqServiceProcess struct {
	Filters filters.Args
}

type RespServiceProcess struct {
	Tasks []swarm.Task
}

type ReqServiceRemove struct {
	ServiceID string
}

type RespServiceRemove struct{}

type ReqServiceUpdate struct {
	ServiceID string
	Service   swarm.ServiceSpec
	Options   types.ServiceUpdateOptions
}

type RespServiceUpdate struct {
	Response types.ServiceUpdateResponse
}

type ReqServiceInspect struct {
	ServiceID      string
	InsertDefaults bool
}

type RespServiceInspect struct {
	Service swarm.Service
	Data    []byte
}
