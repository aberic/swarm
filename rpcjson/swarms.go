package rpcjson

import (
	"context"
	"github.com/aberic/gnomon"
	"gykjgit.dccnet.com.cn/chain/swarm/client"
	"gykjgit.dccnet.com.cn/chain/swarm/rpcjson/model"
)

type Swarm struct{}

// Init Init the swarm.
func (Swarm) Init(req model.ReqSwarmInit, resp *model.RespSwarmInit) error {
	if gnomon.StringIsEmpty(req.Request.ListenAddr) {
		req.Request.ListenAddr = "0.0.0.0:2377"
	}
	response, err := client.ObtainClient().SwarmInit(context.Background(), req.Request)
	if nil != err {
		return err
	}
	resp.Response = response
	return nil
}

// Join Join the swarm.
func (Swarm) Join(req model.ReqSwarmJoin, _ *model.RespSwarmJoin) error {
	return client.ObtainClient().SwarmJoin(context.Background(), req.Request)
}

// Inspect inspects the swarm.
func (Swarm) Inspect(_ model.ReqSwarmInspect, resp *model.RespSwarmInspect) error {
	swarmInfo, err := client.ObtainClient().SwarmInspect(context.Background())
	if nil != err {
		return err
	}
	resp.SwarmInfo = swarmInfo
	return nil
}
